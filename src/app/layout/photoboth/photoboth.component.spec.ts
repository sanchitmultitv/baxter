import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotobothComponent } from './photoboth.component';

describe('PhotobothComponent', () => {
  let component: PhotobothComponent;
  let fixture: ComponentFixture<PhotobothComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotobothComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotobothComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
