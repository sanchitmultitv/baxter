import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BallroomFourComponent } from './ballroom-four.component';

describe('BallroomFourComponent', () => {
  let component: BallroomFourComponent;
  let fixture: ComponentFixture<BallroomFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BallroomFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BallroomFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
