import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import {FormGroup,FormControl} from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-poll-four',
  templateUrl: './poll-four.component.html',
  styleUrls: ['./poll-four.component.scss']
})
export class PollFourComponent implements OnInit {
  pollingLIstfour:any =[];
  poll_id;
  showmsg=true;
  showPoll = false;
  pollForm = new FormGroup({
    polling: new FormControl(''),
  });
  msg;
    constructor(private _fd : FetchDataService,private chat : ChatService) { }
  
    ngOnInit(): void {
      this.chat.getconnect('toujeo-52');
    
    this.chat.getMessages().subscribe((data=>{
      
      console.log('socketdata', data);
      let poll = data;
      let polls = poll.split('_');
      console.log(data)
      if (polls[0] == 'start' && polls[1]=='poll' && polls[3]=='4'){
        this.poll_id = polls[2];
        let audi_id = polls[3]
        console.log(audi_id,"sad");
        this.getPolls();
        setTimeout(() => {
          this.showmsg = false;
          this.showPoll = true;
        }, 1000);
        //  $('.pollModal').modal('show');
      }
      else{
        this.showmsg = true;
        this.showPoll = false;
      }
       console.log('final',this.poll_id);
    }));
    }
    closePopup() {
      $('.pollModalFour').modal('hide');
    }
  getPolls() {
    this._fd.getPollList(this.poll_id).subscribe(res=>{
      this.pollingLIstfour = res.result;
    })
  }
  pollSubmit(id){
    let data = JSON.parse(localStorage.getItem('virtual'));
    // console.log(id);
    // console.log(data.id);
    this._fd.postPoll(id,data.id,this.pollForm.value.polling).subscribe(res=>{
      console.log(res);
      if(res.code == 1){
        this.msg = 'Thank you for submitting your answer';
        this.showPoll = false;
        // this.showmsg = false;
        setTimeout(() => {
          // $('.pollModal').modal('hide');
          this.showmsg = true;
          this.msg = '';
          this.pollForm.reset();
        }, 2000);
        
      }
      this.pollingLIstfour = [];
      
    });
  }
  
}
