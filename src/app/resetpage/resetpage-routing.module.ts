import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResetpageComponent } from './resetpage.component';

const routes: Routes = [{ path: '', component: ResetpageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResetpageRoutingModule { }
