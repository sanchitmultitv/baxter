import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-resetpage',
  templateUrl: './resetpage.component.html',
  styleUrls: ['./resetpage.component.scss']
})
export class ResetpageComponent implements OnInit {

  token;
  msg;
  randno:any;
  //loginForm:FormGroup
  profileForm = new FormGroup({
    email: new FormControl(''),
    pwd: new FormControl(''),
  });

  profileForm2 = new FormGroup({
    password: new FormControl('')
    // otp: new FormControl(''),
  });

  profileForm3 = new FormGroup({
    // email: new FormControl('')
    otp: new FormControl('')
  });

  coverImage = "../../assets/img/h-about.jpg";
  videoPlay = false;
  potrait = false;
  isFoo = false;
  isFood = true;
  isOTP = false;
  resetLINK :any;
  spin = false;
  constructor(private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder:FormBuilder,private route: ActivatedRoute) { 
    // if(localStorage.getItem('virtual')){
    //   this.router.navigateByUrl('/lobby');
    // }
   }

  ngOnInit(): void {
    localStorage.setItem('user_guide', 'start');
    // this.loginForm = this.formBuilder.group({
    //   email: ['',[Validators.email,
    //    Validators.pattern("[^ @]*@[^ @]*"),
    //       emailDomainValidator]]
    // });
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else{
      this.potrait = false;
    }

    
  }

  changeForm(){
    this.isFoo = false;
  this.isFood = true;
  }

  loggedIn() {
    // console.log('logindata', this.profileForm.value);
    if(this.profileForm.value.pwd == 'NSC2021'){
     
      const formData = new FormData();
      
      formData.append('email', this.profileForm.value.email);
      formData.append('pwd', this.profileForm.value.pwd);

     // formData.append('headquarter', this.signupForm.get('job_title').value);
      var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
        
    };
    
     // console.log("password",user.password)
      this.auth.loginMethod(this.profileForm.value.email,this.profileForm.value.pwd).subscribe((res: any) => {
        if (res.code === 1) {
          if( isMobile.iOS() ){
            this.videoPlay = false;
            // this.router.navigateByUrl('/lobby');
          }
          this.videoPlay = false;
          localStorage.setItem('virtual', JSON.stringify(res.result));
          this.isFoo = false;
          this.isFood = true;
          this.videoPlay = false;
          // let vid: any = document.getElementById('myVideo');
          // vid.play();
          if (window.innerHeight>window.innerWidth){
            this.potrait = true;
          }else{
            this.potrait = false;
          }
        } else {
          this.msg = 'Invalid Login';
          this.videoPlay = false;
          this.profileForm.reset();
        }
      }, (err: any) => {
        this.videoPlay = false;
        console.log('error', err)
      });
   
  this.profileForm.reset();
    }else{
      const formData = new FormData();
      
      formData.append('email', this.profileForm.value.email);
      formData.append('pwd', this.profileForm.value.pwd);

     // formData.append('headquarter', this.signupForm.get('job_title').value);
      var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
        
    };
    
     // console.log("password",user.password)
      this.auth.loginMethod(this.profileForm.value.email,this.profileForm.value.pwd).subscribe((res: any) => {
        if (res.code === 1) {
          if( isMobile.iOS() ){
            this.videoPlay = false;
             this.router.navigateByUrl('/lobby');
          }
          this.videoPlay = true;
          localStorage.setItem('virtual', JSON.stringify(res.result));
          // this.isFoo = false;
          // this.isFood = true;
          this.videoPlay = true;
          let vid: any = document.getElementById('myVideo');
          vid.play();
          if (window.innerHeight>window.innerWidth){
            this.potrait = true;
          }else{
            this.potrait = false;
          }
        } else {
          this.msg = 'Invalid Login';
          this.videoPlay = false;
          this.profileForm.reset();
        }
      }, (err: any) => {
        this.videoPlay = false;
        console.log('error', err)
      });
   
  this.profileForm.reset();
    }
    // this._fd.authLogin(user).subscribe(res => {
    //   if (res.code === 1) {
    //     this.videoPlay = true;
    //     localStorage.setItem('virtual', JSON.stringify(res.data));
    //     // this.router.navigate(['/lobby']);
    //     this.videoPlay = true;
    //     let vid: any = document.getElementById('myVideo');
    //     vid.play();
    //   } else {
    //     this.msg = 'Invalid Login';
    //     this.videoPlay = false;
    //     this.loginForm.reset();
    //   }
    // }, err => {
    //   this.videoPlay = false;
    //   console.log('error', err)
    // });
  }

  updatePass(){
    // this.spin=true;
    //  this.randno = Math.floor(1000 + Math.random() * 9000);
    this.spin=true
    const formData = new FormData();
      formData.append('password', this.profileForm2.value.password);
      
    this.resetLINK = this.route.snapshot.queryParamMap.get('email');
      // let data = JSON.parse(localStorage.getItem('virtual'));
      
    this._fd.updatePassword(this.resetLINK,this.profileForm2.value.password).subscribe((res=>{
      this.spin=true
      if (res.code === 1) {
        this.spin=false}
      console.log(res);
      this.isFood = false;
    this.isFoo = true;

      // this.qaList = res.result;
      // alert('hello');
    }))
  }

//   submitOTP(){
//     this.randno = Math.floor(1000 + Math.random() * 9000);

// console.log(this.randno);

//    const formData = new FormData();
//      formData.append('email', this.profileForm2.value.email);
   
//      var OTPmain = this.randno
//    this._fd.submitOTP(this.profileForm2.value.email,btoa(OTPmain)).subscribe((res=>{
//      console.log(res);
//      this.isFood = false;
//    this.isOTP = true;

//    }))
//  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else{
      this.potrait = false;
    }
  }
  
  endVideo() {
    // this.videoPlay = false;
    // this.potrait = false;
    this.router.navigateByUrl('/lobby');
    let welcomeAuido:any = document.getElementById('myAudio');
    welcomeAuido.play();
  }
  skipButton() {
    let pauseVideo: any = document.getElementById("myVideo");
    // pauseVideo.currentTime = 0;
    // pauseVideo.pause();
    this.videoPlay=false;
    this.router.navigateByUrl('/lobby');
  }
}