import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
declare var introJs: any;
declare var $: any;
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy, AfterViewInit {
  videoEnd = false;
  receptionEnd = false;
  showVideo = false;
  auditoriumLeft = false;
  auditoriumRight = false;
  exhibitionHall = false;
  registrationDesk = false;
  networkingLounge = false;
  intro: any;
  player: any;
  actives: any = [];
  liveMsg = false;
  showPoster = false;
  // timer:false;
  videoPlay2: any;

  // videoUrl;
  @ViewChild('recepVideo', { static: true }) recepVideo: ElementRef;

  constructor(private router: Router, private _fd: FetchDataService, private chat: ChatService) { }
   videoPlayer = 'https://d3ep09c8x21fmh.cloudfront.net/fmc/LobbyVideo.mp4';
  ngOnInit(): void {
    // let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();

    // if(timer >= '10:40:20'){
    //   alert(timer)
    // }
    if (localStorage.getItem('user_guide') === 'start') {
      this.getUserguide();
    }
    this.stepUpAnalytics('click_lobby');
    this.audiActive();
    this.chat.getconnect('toujeo-140');
    this.chat.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'start_live') {
        this.liveMsg = true;
      }
      if (data == 'stop_live') {
        this.liveMsg = false;
      }

    }));

    let playVideo: any = document.getElementById("playVideo");
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime = 0;
        pauseVideo.pause();
      }
    }
  }

  endPlayscreen() {
    this.showPoster = true;
  }
  playScreenOnImg() {
    this.showPoster = false;
    let playVideo: any = document.getElementById("playVideo");
    playVideo.play();
  }
  ngAfterViewInit() {
    this.playAudio();
  }
  openModalVideo() {
    $('#playVideo').modal('show');
    let vid: any = document.getElementById("video");
    vid.play();
  }
  closeModalVideo() {
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
  closePopup() {
    $('.yvideo').modal('hide');
    $('.pic1').modal('hide');
    $('.pic2').modal('hide');
    $('.pic3').modal('hide');
    $('.pic4').modal('hide');
    $('.note1').modal('hide');
    $('.note2').modal('hide');
    $('.note3').modal('hide');
  }
  openpopup() {
    if (localStorage.getItem('whatever')) {
      $('.NoFeedback').modal('show');
    } else {
      $('.feebackModal').modal('show');
    }
  }

  closePopuptwo() {
    $('.NoFeedback').modal('hide');
  }

  audiActive() {
    this._fd.activeAudi().subscribe(res => {
      this.actives = res.result;
    })
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    // if (window.innerWidth <= 767) {
    //   this.player.resize({
    //     width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
    //   });

    // } else {
    //   this.player.resize({
    //     width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
    //   });
    // }
  }
  playAudio() {
    // localStorage.setItem('play', 'play');
    // let abc: any = document.getElementById('myAudio');
    // abc.play();
    // alert('after login audio')
  }
  playevent() {
    this.videoEnd = true;
  }
  playReception() {
    let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // alert(timer)
    // if (timer >= '18:59:59') {
      // alert('')
      this.receptionEnd = true;
      this.showVideo = true;
      let vid: any = document.getElementById("recepVideo");
      vid.play();
    // }else {
    //   $('.note3').modal('show');
    // }
    
    // let pauseVideo: any = document.getElementById("video");
    // pauseVideo.currentTime = 0;
    // pauseVideo.pause();
  }
  receptionEndVideo() {
    this.router.navigate(['/auditorium/five']);

  }
  gotoKeyPortfolio() {
    this.registrationDesk = true;
    this.showVideo = true;
    let vid: any = document.getElementById("regDeskvideo");
    vid.play();
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
  }
  onEndKeyPortfolio() {
    this.router.navigate(['/keyportfolio']);

  }

  // gotoAuditoriumFront() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }

  playAuditoriumLeft() {
    // if (this.actives[3].status == true) {
      // let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // if (timer >= '8:59:59' && timer <= '17:59:59') {
      this.auditoriumLeft = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audLeftvideo");
      vid.play();
    // }
    // else if (timer <= '8:58:59') {
      // $('.note1').modal('show');
    // }
    // else if (timer >= '18:01:01') {
      // $('.note2').modal('show');
    // }
    // let pauseVideo: any = document.getElementById("video");
    // pauseVideo.currentTime = 0;
    // pauseVideo.pause();
  }
  // else {
  //   $('.audiModal').modal('show');
  // }

  // }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/networkingLounge']);
  }
  playAuditoriumRight() {
    // if (this.actives[2].status == true) {
    let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // if (timer >= '8:59:59' && timer <= '17:59:59') {
      this.auditoriumRight = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audRightvideo");
      vid.play();
    // }
    // else if (timer <= '8:58:59') {
    //   $('.note1').modal('show');
    // }
    // else if (timer >= '18:01:01') {
    //   $('.note2').modal('show');
    // }
    // let pauseVideo: any = document.getElementById("video");
    // pauseVideo.currentTime = 0;
    // pauseVideo.pause();
    // }
    // else {
    //   $('.audiModal').modal('show');
    // }
  }
  gotoAuditoriumRightOnVideoEnd() {
    this.router.navigate(['/capturePhoto']);
  }
  playExhibitionHall() {
    // let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    //  alert(timer)
    // if (timer >= '8:59:59' && timer <= '17:59:59') {
      this.exhibitionHall = true;
      this.showVideo = true;
      let vid: any = document.getElementById("exhibitvideo");
      vid.play();
      // let pauseVideo: any = document.getElementById("video");
      // pauseVideo.currentTime = 0;
      // pauseVideo.pause();
    // } 
    // else if (timer <= '8:58:59') {
    //   $('.note1').modal('show');
    // }
    // else if (timer >= '18:01:01') {
      // $('.note2').modal('show');
    // }
  }

  gotoExhibitionHallOnVideoEnd() {
    this.router.navigate(['/auditorium/one']);
  }
  // playRegistrationDesk() {
  //   this.registrationDesk = true;
  //   this.showVideo = true;
  //   let vid: any = document.getElementById("regDeskvideo");
  //   vid.play();
  // }
  // gotoRegistrationDeskOnVideoEnd() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }
  playNetworkingLounge() {
    let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // alert(timer)
    // if (timer >= '8:59:59' && timer <= '17:59:59') {
      this.networkingLounge = true;
      this.showVideo = true;
      let vid: any = document.getElementById("netLoungevideo");
      vid.play();
      // let pauseVideo: any = document.getElementById("video");
      // pauseVideo.currentTime = 0;
      // pauseVideo.pause();
    // } else if (timer <= '8:58:59') {
    //   $('.note1').modal('show');
    // }
    // else if (timer >= '18:01:01') {
      // $('.note2').modal('show');
    // }
  }
  gotoNetworkingLoungeOnVideoEnd() {
    this.router.navigate(['/exhibitionHall/life']);
  }
  lightbox_open() {
    //this.videoUrl = video;
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    lightBoxVideo.play();

  }
  lightbox_close() {
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    lightBoxVideo.pause();
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    const formData = new FormData();
    formData.append('event_id', '140');
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', 'others');
    formData.append('designation', 'others');
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  getUserguide() {
    this.intro = introJs().setOptions({
      hidePrev: true,
      hideNext: true,
      exitOnOverlayClick: false,
      exitOnEsc: false,
      steps: [
        {
          element: document.querySelector("#reception_pulse"),
          intro: "<div style='text-align:center'>Click here to view standee</div>"
        },
        {
          element: document.querySelectorAll("#networking_pulse")[0],
          intro: "<div style='text-align:center'>Click here to view exhibition stalls</div>"
        },
        {
          element: document.querySelectorAll("#exhibition_pulse")[0],
          intro: "<div style='text-align:center'>Click here to join auditorium</div>"
        },
        {
          element: document.querySelectorAll("#audLeft_pulse")[0],
          intro: "<div style='text-align:center'>Click here to interact in the networking lounge</div>"
        },
        {
          element: document.querySelectorAll("#audRight_pulse")[0],
          intro: "<div style='text-align:center'>Click here to click pictures</div>"
        },
        {
          element: document.querySelector("#frontaudi_pulse"),
          intro: "<div style='text-align:center'>Click here to view standee</div>"
        },
        {
          element: document.querySelectorAll("#heighlight_pulse")[0],
          intro: "<div style='text-align:center'>Click here to view standee</div>"
        },
        {
          element: document.querySelectorAll("#agenda_pulse")[0],
          intro: "<div style='text-align:center'>Click here to view standee</div>"
        },
        {
          element: document.querySelectorAll("#regDeskvideo")[0],
          intro: "<div style='text-align:center'>Click here for platform related issues</div>"
        },
        // {
        //   element: document.querySelectorAll("#gala")[0],
        //   intro: "<div style='text-align:center'>Click here for gala evening</div>"
        // }


      ]
    }).oncomplete(() => document.cookie = "intro-complete=true");

    let start = () => this.intro.start();
    start();
    // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
    //   window.setTimeout(start, 1000);
  }
  skipButton() {

    this.networkingLounge=false;
    this.router.navigateByUrl('/exhibitionHall/life');
  }
  skipButton2() {
    this.receptionEnd = false;
    this.router.navigateByUrl('/auditorium/five');
  }
  skipButton3() {
    this.exhibitionHall = false;
    this.router.navigateByUrl('/auditorium/one');
  }
  skipButton4() {
    this.auditoriumRight = false;
    this.router.navigateByUrl('/capturePhoto');
  }
  skipButton5() {
    this.auditoriumLeft = false;
    this.router.navigateByUrl('/networkingLounge');
  }
  playGrowBySharingVideo() {
    // this.videosource = video;
    let playVideo: any = document.getElementById("videop");
    playVideo.play();
    $('#playVideos').modal('show');
  }
  closeModalVideos() {
    let pauseVideo: any = document.getElementById("videop");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideos').modal('hide');
  }
  ngOnDestroy() {
    if (localStorage.getItem('user_guide') === 'start') {
      let stop = () => this.intro.exit();
      stop();
    }
    localStorage.removeItem('user_guide');
  }


}
