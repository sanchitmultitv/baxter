import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ToastrService } from 'ngx-toastr';

declare var $: any;
@Component({
  selector: 'app-networking-lounge',
  templateUrl: './networking-lounge.component.html',
  styleUrls: ['./networking-lounge.component.scss']
})
export class NetworkingLoungeComponent implements OnInit, OnDestroy{
  videoEnd = false;
  liveMsg = false;
  ChatMsg = false;
  senderName;
  resetLINKS: any;
  sender_id: any;
  sender_name;
  searchChatList = [];
  receiver_id: any;
  chatMessage = [];
  allChatList = [];
  timer;
  messageList: any=[];
  roomName= 'fmc';
  datas:any;
  commentsList = [];
  commentsListing = [];
  textMessage = new FormControl('');
  type = new FormControl('');
  interval;
  typ = 'normal';
  oneToOneChatList = [];
  allChatIndex = 0;
  chatUser: any;
  receiver_name;
  data: any;
  videoPlayer = '../assets/video/networking_lounge_video.mp4';
  constructor(private _fd: FetchDataService,private router: Router, private chat: ChatService, private route: ActivatedRoute, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.networksend();
    this.getAllAttendees();
// this.chatGroupTwo();
    // this.getAllAttendees()
    this.chat.getconnect('toujeo-140');
    this.chat.getMessages().subscribe((data => {
      //  console.log('data',data);
      let check = data.split('_');
      console.log(data, 'okkk');
      let datas = JSON.parse(localStorage.getItem('virtual'));
      this.chatUser = check[2];
      if (check[0] == 'one2one' && check[1] == datas.id) {
        
        this.toastr.success(this.chatUser + ' sent you a message');
    $('.chatsModal').modal('show');
        
        // this.toastr.onHidden = function() { console.log("onHide"); };

        this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
          this.chatMessage = res.result;
        });
      }
      if(check[0] == 'groupchat'){
        // alert("GROUP CHAT")
       this.chatGroupTwo()
      }
      if (data == 'start_live') {
        this.liveMsg = true;
      }
      if (data == 'stop_live') {
        this.liveMsg = false;
      }
      else {
        let getMsg = data.split('_');
        console.log('chats', getMsg);
        // if (getMsg[0] == "one" && getMsg[1] == "to" && getMsg[2] == "one") {
        //   let data = JSON.parse(localStorage.getItem('virtual'));
        //   if (getMsg[3] == data.id) {
        //     this.senderName = getMsg[4];
        //     this.ChatMsg = true;
        //     setTimeout(() => {
        //       this.ChatMsg = false;
        //     }, 5000);
        //   }
        // }
      }
    }));
    let playVideo: any = document.getElementById("playVideo");
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime = 0;
        pauseVideo.pause();
      }
    }
  }
  loadData(): void {
    this.chatGroupTwo();
    this.chat.getconnect('toujeo-140');
    // this.chat.getMessages().subscribe((data => {
    //   if (data == 'group_chat') {
    //     this.chatGroupTwo();
    //   }
    // }));
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chat.addUser(data.name, this.roomName);
    localStorage.setItem('username', data.name);
    this.chat
      .receiveMessages(this.roomName)
      .subscribe((msgs: any) => {    
        console.log('demo', this.messageList);
      });
  }
  chatGroupTwo() {
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.datas = data.id;
    this.datas = data.name;
    this._fd.groupchating().subscribe(res => {
      console.log('res', res);
      this.messageList = res.result;
      $('.groupchatTwo').modal('show');
      // $('.groupchatsModal').modal('toggle');
      // $('.groupchatsModal').trigger('click');

    });
  }
  networksend() {
    let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    this.resetLINKS = this.router.url;
     let data = JSON.parse(localStorage.getItem('virtual'));

    const formData = new FormData();
     formData.append('event_id', '140');
     formData.append('user_id', data.id );
     formData.append('name', data.name );
     formData.append('email', data.email );
     formData.append('page', this.resetLINKS );
     formData.append('created', timer);

    // let data = JSON.parse(localStorage.getItem('virtual'));
    // this.resetLINKS = this.router.url;

    // console.log("707007"+this.router.url);
    this._fd.postNetwork(formData).subscribe((res => {
      if (res.code === 1) {
        // alert('Submitted Succesfully');
      }
    }))

  }
  networkremove(){
    this.resetLINKS = this.router.url;
    let data = JSON.parse(localStorage.getItem('virtual'));

    const formData = new FormData();
    formData.append('event_id', '140');
    formData.append('user_id', data.id );
    formData.append('page', '/networkingLounge' );

    this._fd.removenetwork(formData).subscribe((res:any) => {
      if (res.code === 1) {
        //  alert('Submitted Succesfully');
      }
    })
  }
  
  getAllAttendees() {

    let event_id = 140;
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    this._fd.GetNetworkpage(event_id).subscribe((res: any) => {
      this.allChatList = res.result;
      this.searchChatList = res.result;
      this.receiver_id = res.result[0].user_id;
      this.receiver_name = res.result[0].name;
      this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
        this.chatMessage = res.result;
      });

      // this.timer = setInterval(() => {
      //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      //     this.chatMessage = res.result;
      //   });

      // }, 150000);
      this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
        this.chatMessage = res.result;
      });

    });
    $('.chatsModal').modal('show');

  }
  showVideoPopup() {
    $('#playVideo').modal('show');
    let player: any = document.getElementById("video");
    player.play();
  }
  closeModalVideo() {
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
  videoEnded() {
    this.videoEnd = true;
  }
  openWtsapp() {
    $('.wtsappModal').modal('show');
  }
  openChat() {
    $('.chatsModal').modal('show');
  }
  openCamera() {
    this.router.navigate(['/capturePhoto']);
  }
  


  // all(){
  //   let event_id = 140;
  //   this._fd.GetNetworkpage(event_id).subscribe((res: any) => {
  //     if (res.code === 1) {
  //     }
  //   })
  // }

  searchElement(query) {
    let event_id = 140;
    this._fd.getOne2oneChatList(event_id, query).subscribe(res => {
      this.allChatList = res.result;
    });
  }

  selectedChat(chat, ind) {
    // alert(chat)
    // alert(ind)
    this.allChatIndex = ind;
    let event_id = 140;
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    this.receiver_id = chat.user_id;
    this.receiver_name = chat.name;
    console.log(chat)
    this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      this.chatMessage = res.result;
    });
  }
  postMessageTwo(value) {
    // console.log(value);
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.datas = data.id;
    this.datas = data.name;

    const formData = new FormData();
    formData.append('room_name', this.roomName);
    formData.append('user_name', data.name);
    formData.append('email', data.email);
    formData.append('chat_data', value);
    formData.append('is_approved', '1');
    formData.append('event_id', '140');
    formData.append('created', '2020-12-10 18:18:18');

    this._fd.postGroup(formData).subscribe(res => {
      console.log('res', res);
      // this.messageList = res.result;
    });

    
    
    // console.log(data.name);
    // this.chatService.sendMessage(value, data.name, this.roomName);
    // console.log(this.roomName);
    this.textMessage.reset();
    // this.loadData();
    //this.newMessage.push(this.msgs);
  }
  postOneToOneChat(event) {
    let msg = event.value;
    const formData = new FormData();
    formData.append('sender_id', this.sender_id);
    formData.append('sender_name', this.sender_name);
    formData.append('receiver_id', this.receiver_id);
    formData.append('receiver_name', this.receiver_name);
    formData.append('msg', msg);
    if (event.value !== null) {
      this._fd.postOne2oneChat(formData).subscribe(data => {
        this.textMessage.reset();
        this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
          this.chatMessage = res.result;

        });
      });
    }
    setTimeout(() => {
      $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight - 100;
    }, 1500);
    // this.timer = setInterval(() => {
    //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
    //     this.chatMessage = res.result;
    //   });
    // }, 1000);
  }

  getComments() {
    let event_id = 53;
    let user_id = 1;
    let type = 'normal';
    this._fd.getComments(event_id, user_id, type).subscribe(res => {
      this.commentsList = res.result;
      this.commentsListing = res.result;
    });
  }
  getType(value) {
    console.log(value);
    let event_id = 53;
    let user_id = 1;
    this.typ = value;
    this._fd.getComments(event_id, user_id, value).subscribe(res => {
      this.commentsList = res.result;
    })
  }
  closePopup() {
    $('.chatsModal').modal('hide');
    $('.groupchatTwo').modal('hide');
  }
  ngOnDestroy() {
    this.networkremove();
    }
  }